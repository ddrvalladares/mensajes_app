/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ddrvalladares.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Dídier Valladares
 */
public class Conexion {
    
    public Connection get_connection(){
        Connection conexion = null;
        try{
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app?serverTimezone=UTC","root","I34n34G37");
            
            if(conexion != null){
                System.out.println("Conexión Exitosa");
            }
        }catch(SQLException e){
            System.out.println("Error de Conexion: " + e);
        }

        return conexion;
    }
    
}
